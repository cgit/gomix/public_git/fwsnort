%global fwsnortlogdir /var/log/fwsnort

### get the first @INC directory that includes the string "linux".
### This may be 'i386-linux', or 'i686-linux-thread-multi', etc.
%define fwsnortmoddir `perl -e '$path='i386-linux'; for (@INC) { if($_ =~ m|.*/(.*linux.*)|) {$path = $1; last; }} print $path'`

Summary: Translates Snort rules into equivalent iptables rules
Name: fwsnort
Version: 1.0.6
Release: 3%{?dist}
License: GPLv2+
Group: System Environment/Daemons
Url: http://www.cipherdyne.org/fwsnort/
Source0: http://www.cipherdyne.org/fwsnort/download/fwsnort-1.0.6.tar.gz
Source1: logrotate.fwsnort
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch: noarch 
Requires: iptables,perl-Net-IPv4Addr, perl-IPTables-Parse, perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))

%description
fwsnort translates Snort rules into equivalent iptables rules and generates
a Bourne shell script that implements the resulting iptables commands. This
ruleset allows network traffic that exhibits Snort signatures to be logged
and/or dropped by iptables directly without putting any interface into
promiscuous mode or queuing packets from kernel to user space. In addition,
fwsnort (optionally) uses the IPTables::Parse module to parse the iptables
ruleset on the machine to determine which Snort rules are applicable to the
specific iptables policy.  After all, if iptables is blocking all inbound
http traffic from external addresses, it is probably not of much use to try
detecting inbound attacks against against tcp/80. By default fwsnort
generates iptables rules that log Snort sid's with --log-prefix to klogd
where the messages can be analyzed with a log watcher such as logwatch or
psad (see http://www.cipherdyne.org/psad). fwsnort relies on the iptables
string match module to match Snort content fields in the application portion
of ip traffic. Since Snort rules can contain hex data in content fields,
fwsnort implements a patch against iptables-1.2.7a which adds a
"--hex-string" option which will accept content fields such as
"|0d0a5b52504c5d3030320d0a|". fwsnort is able to translate approximately 60%
of all rules from the Snort-2.3.3 IDS into equivalent iptables rules. For
more information about the translation strategy as well as
advantages/disadvantages of the method used by fwsnort to obtain intrusion
detection data, see the README included with the fwsnort sources or browse
to: http://www.cipherdyne.org/fwsnort/

%prep
%setup -q
mv deps/snort_rules/VERSION SNORT-RULES-VERSION
cp -p %SOURCE1 .

%build

%install
rm -rf $RPM_BUILD_ROOT
### log directory
mkdir -p $RPM_BUILD_ROOT%fwsnortlogdir

### fwsnort config
mkdir -p $RPM_BUILD_ROOT%_sysconfdir/fwsnort

mkdir -p $RPM_BUILD_ROOT%_bindir
mkdir -p $RPM_BUILD_ROOT%{_mandir}/man8
mkdir -p $RPM_BUILD_ROOT%_sbindir

install -m 755 fwsnort $RPM_BUILD_ROOT%_sbindir/
install -m 644 fwsnort.conf $RPM_BUILD_ROOT%_sysconfdir/fwsnort/
install -m 644 fwsnort.8 $RPM_BUILD_ROOT%{_mandir}/man8/

### install snort rules files
cp -r deps/snort_rules $RPM_BUILD_ROOT%_sysconfdir/fwsnort

mkdir -p $RPM_BUILD_ROOT/etc/logrotate.d
install -p -m 644  logrotate.fwsnort $RPM_BUILD_ROOT/etc/logrotate.d/fwsnort

%clean
rm -rf $RPM_BUILD_ROOT

#%pre
### not used

%post
### not used

%preun
### not used

%files
%defattr(-,root,root)
%doc LICENSE VERSION README CREDITS TODO SNORT-RULES-VERSION
%dir %fwsnortlogdir
%_sbindir/*
%{_mandir}/man8/*

%dir %_sysconfdir/fwsnort
%config(noreplace) %_sysconfdir/fwsnort/fwsnort.conf

%dir %_sysconfdir/logrotate.d
%config(noreplace) %_sysconfdir/logrotate.d/fwsnort

%dir %_sysconfdir/fwsnort/snort_rules
%config(noreplace) %_sysconfdir/fwsnort/snort_rules/*

%changelog
* Thu Feb 04 2010 Guillermo Gómez <ggomez@neotechgw.com> - 1.0.6-3
- License adjusted to GPLv2+

* Wed Feb 03 2010 Guillermo Gómez <ggomez@neotechgw.com> - 1.0.6-2
- documentation included, LICENSE VERSION README CREDITS TODO
  SNORT-RULES-VERSION

* Sat Jan 2 2010 Guillermo Gómez <ggomez@neotechgw.com> - 1.0.6-1
- First Fedora spec compliant version, several modifications
- No deps included
- Free snort rules included
